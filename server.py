from math import pi

import pandas as pd

from bokeh.io import curdoc
from bokeh.palettes import Category20c
from bokeh.plotting import figure
from bokeh.models import TextInput, Button, RadioButtonGroup
from bokeh.layouts import column, row
from bokeh.transform import cumsum

import requests, json

### For the plot to be dislayed at first load of page ###
def default_plot():
    try:
        repo = text_input.value
        r = requests.get('https://api.github.com/repos/' + repo + '/contributors')
        json_data = r.json()
        contributions_by_user = {}
        for contributions in json_data:
            contributions_by_user[contributions["login"]] = contributions["contributions"]
        top_contributors = sorted(contributions_by_user.items(), key=lambda x:x[1], reverse=True)
        
        label_list = []
        data_list =[]

        visitor = "Other"
        visitors = 0
        visitor_contri = 0.0

        num_of_named_contributors = 0

        for key, value in top_contributors:
            if num_of_named_contributors < 10:
                label_list.append(key)
                data_list.append(value)
                num_of_named_contributors += 1
            else:
                visitor_contri += value
                visitors += 1

        visitor += "(" + str(visitors) + ")"
        label_list.append(visitor)
        data_list.append(visitor_contri)

        plot = figure(x_range=label_list, plot_height=800, plot_width=800, title="Contributions", toolbar_location=None, tools="")

        plot.vbar(x=label_list, top=data_list, width = 0.9)

        plot.xgrid.grid_line_color = None
        plot.y_range.start = 0
        curdoc().clear()
        layout2 = column(layout, plot, sizing_mode="stretch_both")
        curdoc().add_root(layout2)
    except TypeError:
        curdoc().add_root(invalid_button)

### Callback for when a new repo is wanted and the contributions radio is active ###
def contributions_callback():
    try:
        repo = text_input.value
        r = requests.get('https://api.github.com/repos/' + repo + '/contributors')
        json_data = r.json()
        contributions_by_user = {}
        for contributions in json_data:
            contributions_by_user[contributions["login"]] = contributions["contributions"]
        top_contributors = sorted(contributions_by_user.items(), key=lambda x:x[1], reverse=True)
        
        label_list = []
        data_list =[]

        visitor = "Other"
        visitors = 0
        visitor_contri = 0.0

        num_of_named_contributors = 0

        for key, value in top_contributors:
            if num_of_named_contributors < 10:
                label_list.append(key)
                data_list.append(value)
                num_of_named_contributors += 1
            else:
                visitor_contri += value
                visitors += 1

        visitor += "(" + str(visitors) + ")"
        label_list.append(visitor)
        data_list.append(visitor_contri)

        plot = figure(x_range=label_list, plot_height=800, plot_width=800, title="Contributions", toolbar_location=None, tools="")

        plot.vbar(x=label_list, top=data_list, width = 0.9)

        plot.xgrid.grid_line_color = None
        plot.y_range.start = 0
        curdoc().clear()
        layout2 = column(layout, plot, sizing_mode="stretch_both")
        curdoc().add_root(layout2)
    except TypeError:
        curdoc().add_root(invalid_button)

### Callback for when a new repo is entered and the languages radio is active ###
def languages_callback():
    try:
        repo = text_input.value
        r = requests.get('https://api.github.com/repos/' + repo + '/languages')
        languages = json.loads(r.text)

        data = pd.Series(languages).reset_index(name='value').rename(columns={'index':'lang'})
        data['angle'] = data['value']/data['value'].sum() * 2*pi
        
        if len(languages) < 2:
            data['color'] = ["#1f77b4"]
        elif len(languages) < 3:
            data['color'] = ["#1f77b4", "#aec7e8"]
        else:
            data['color'] = Category20c[len(languages)]

        pie = figure(plot_height=400, title="languages", toolbar_location=None, tools="hover", tooltips="@lang: @value", x_range=(-0.5, 1.0))
        pie.wedge(x=0, y=1, radius=0.4, start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'), line_color="white", fill_color='color', legend_field='lang', source=data)

        pie.axis.axis_label=None
        pie.axis.visible=False
        pie.grid.grid_line_color=None
        curdoc().clear()
        layout2 = column(layout,pie)
        curdoc().add_root(layout2)
    except TypeError:
        curdoc().add_root(invalid_button)

### Master callback for when a new repo is entered ###
def callback(attr, old, new):
    if radio_button_group.active == 0:
        contributions_callback()
    else:
        languages_callback()

### Master callback for when a radio is clicked ###
def radio_callback(attrname):
    if radio_button_group.active == 0:
        contributions_callback()
    else:
        languages_callback()

### text box to type in repo ###
text_input = TextInput(value="archlinux/devtools", title="Repo:", sizing_mode="fixed")
text_input.on_change("value", callback)

### radio button group that will allow the user to select whether they want to view the contributions or languages used ###
LABELS = ["Contributions", "Languages"]

radio_button_group = RadioButtonGroup(labels=LABELS, active=0, sizing_mode="fixed")
radio_button_group.on_click(radio_callback)

### display that the repo entered was invalid
invalid_button = Button(label="Invalid Repo", button_type="danger", sizing_mode="fixed")

### Creating and displaying repo ###
layout =  column(row(text_input, sizing_mode="stretch_both"), radio_button_group)
curdoc().add_root(layout)
curdoc().title = "Github Repo Data"

default_plot()