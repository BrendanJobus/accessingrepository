import requests, json
import os, sys, argparse
from termgraph import termgraph as tg

def get_repos_contributors(username):
    """ 
    Calls  https get request for the contributors of the arch linux devtools
    Returns a python dict of the json data that was sent from github
    """
    r = requests.get('https://api.github.com/repos/' + username + '/contributors')
    return r.json()

def parse_for_most_contributions(json_data):
    """
    Creates an ordered python dict top_contributors with the user login as the key and the number of contributions as the value
    """
    contributions_by_user = {}
    for contributions in json_data:
        contributions_by_user[contributions["login"]] = contributions["contributions"]
    top_contributors = sorted(contributions_by_user.items(), key=lambda x:x[1], reverse=True)
    return top_contributors

def print_contributor_data(data):
    """
    Prints the contributor data as a graph into the console, groups the contributors who have only contributed 9 or less times into the Other category 
    uses the termgraph utility found in https://github.com/mkaz/termgraph
    """
    label_list = []
    data_list = []

    visitor = "Other"
    visitors = 0
    visitor_contri = 0.0
    for key, value in data:
        if value >= 10.0:
            new_data = []
            new_data.append(value)
            label_list.append(key)
            data_list.append(new_data)
        else:
            visitor_contri += value
            visitors += 1

    visitor += "(" + str(visitors) + ")"
    new_data = []
    new_data.append(visitor_contri)
    label_list.append(visitor)
    data_list.append(new_data)

    normaled = tg.normalize(data_list,150)
    len_categories = 2
    args = {'title': None, 'width': 50,
        'format': '{:<5.2f}', 'suffix': '', 'no_labels': False,
        'color': None, 'vertical': False, 'stacked': True,
        'different_scale': False, 'calendar': False,
        'start_dt': None, 'custom_tick': '', 'delim': '',
        'verbose': False, 'version': False}
    colors = [94]
    tg.stacked_graph(label_list, data_list, normaled, len_categories, args, colors)

def main():
    if len(sys.argv) == 1:
        os.system("start https://localhost:5006/server")
        os.system("bokeh serve --show server.py")
    else:
        text = "A base compilation will start up a bokeh server that will display interactive graphs and data in localhost:5006/server"
        parser = argparse.ArgumentParser(description=text)
        parser.add_argument("-v", "--version", help="show program version", action="store_true")
        parser.add_argument("-c", help ="Runs a base call to the archlinux devtools repos and displays a graph of the contributions on the command line", action="store_true")
        args = parser.parse_args()

        if args.version:
            print("This is myprogram version 1.0")

        if args.c:
            r = get_repos_contributors('archlinux/devtools')
            top_contributors = parse_for_most_contributions(r)
            print ("Top Contributor is " + str(top_contributors[0][0]) + " with " + str(top_contributors[0][1]) + " contributions")
            print_contributor_data(top_contributors)
            sys.exit()

if __name__ == "__main__":
    main()