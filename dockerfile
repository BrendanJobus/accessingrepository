FROM python:3.9.1-slim-buster

ADD accessing.py /
ADD server.py /

RUN pip install requests
RUN pip install bokeh
RUN pip install pandas
RUN pip install termgraph

CMD [ "python", "./accessing.py" ]