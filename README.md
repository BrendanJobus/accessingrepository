# Accessing Github SWENG Project #

My assignment can be used as both a command line tool and a program that creates a server on a localhost that will take data from the github api to create interactive graphs. As a command line tool, it uses [termgraph](https://github.com/mkaz/termgraph) to create a graph on the command line of all the contributers to the [archlinux devtools repo](https://github.com/archlinux/devtools) (though anyone who has commited less that 10 times is linked together into the *_other_* category). The bokeh server is run on `localhost:5006/server`, and displays both the contributors to a repo (though this time it is only the top 10 contributors and then they are grouped into the *_other_* category) and the languages used in the repo, the repo that you are looking at can be changed to whatever repo you want, though by default it is looking at `archlinux/devtools` again.


## Assignments ##
This repository holds both the _Accessing Respository REST API_ assignment and the _Software Engineering Metric Visualisation Proejct_.

The first assignment is from commits `be8635a` to `8817c1f` and the second assignment is everything past that.


## Installation ##
### Server ###
If docker is installed, all that is needed is the respective start script for your machine, `start.sh` for linux, and `startWindows.bat` for windows, this will create a docker container for the server and run it.

If docker is not installed, the following dependencies must be installed:

- Python\*
- Requests
- termgraph
- Bokeh
- Pandas

\* sybolises that the dependency is not installed with `pip install dependency_name`

### Command Line ###
To run the program only through the command line, the repo will have to be cloned and compiled with `python accessing.py -c`, however, the `termgraph` dependency should be the only dependency required so long as you are only attempting to run the program through the command line.

## Usage ##
Running the server through docker is as simple as running `./start.sh` or by double clicking `startWindows.bat` after downloading the respective file for your system, this should start the server and open it in your browser, if it does not load the page at first, click try again, this is due to the webpage being opened before docker finishes creating the server, it is dependent on many things and may just work however.


If you have all the dependencies and wish to simply compile it on the command line, `python accessing.py` will run the server and open `https://localhost:5006/server` in your browser, however, you will need `server.py` in order to run it this time.


To run the program in the command line, you will need the `accessing.py` file, then running `python accessing.py -c` will run the base case.


With `accessing.py` installed you can run `python accessing.py -h` or `python accessing.py --help` for help.